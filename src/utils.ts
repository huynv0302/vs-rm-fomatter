import * as vscode from "vscode";
import { FunctionRanger } from "./constants";

export const FindFunctionRanger = (text: string) => {
    for (let i = 0; i < FunctionRanger.length; i++) {
        if (text.trim().replace(/\s/gi, "").indexOf(`${FunctionRanger[i].start}(`) == 0) {
            return FunctionRanger[i];
        }
    }
    return;
};

export const FindPositionFunction = (
    values: {
        start_line: number,
        start_script: string,
        end_script: string,
        lineCount: number,
        document: vscode.TextDocument
    }
) => {
    const { start_line, start_script, end_script, lineCount, document } = values;

    let start_count = 0,
        end_count = 0,
        end_line = start_line - 1;

    for (let i = start_line; i < lineCount; i++) {
        end_line++;
        let line = document.lineAt(i);
        let text = line.text.trim().replace(/\s/gi, "");

        if (text.indexOf(start_script + "(") == 0) start_count++;
        if (text == end_script) end_count++;
        if (text == end_script && start_count == end_count) {
            break;
        }
    }

    return {
        start_count,
        end_count,
        end_line,
    };
};

export const TextTrim = (text: string) => {
    return text.trim().replace(/\s/gi, "");
}

export const GetFunctionNameInString = (text: string) => {
    let list_funcs = text && text.match(/([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/g) || [];
    return list_funcs.filter((item) => item[0] === item[0].toUpperCase())
}

export const MatchVaribales = (text: string) => {
    return text.match(/\$\{(.*?)\}/gi)
}

export const GetFunctionDefined = (document: vscode.TextDocument) => {
    const lineCount = document.lineCount;
    const definedFuntions: any = {};

    for (let i = 0; i < lineCount; i++) {
        let line = document.lineAt(i);
        let text = line.text;

        let funcs = GetFunctionNameInString(text);

        funcs.map((func) => {
            if(func == "DefineFunc"){
                try{
                    let values = text.match(/\((.*?)\)/gi);
                    if(values){
                        let params = values[0].replace("(", "").replace(")", "").split(",").map((item) => item.trim());
                        if(params.length){
                            let id = (params[0] as string).replace(/\"/gi, "");
                            definedFuntions[id] = {
                                title: `(defined) ${id}(${params.slice(1).join(",")}): any`,
                                content: params.slice(1).map((item) => {
                                    return `@param ${item.replace(/\"/gi, "")} - any`
                                }).concat([
                                    "@return - any"
                                ])
                            }
                        }
                    }
                } catch(err){
                    console.log(err);
                }
            }
        })
    }

    return definedFuntions
}