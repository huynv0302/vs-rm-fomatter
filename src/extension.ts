// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import * as lodash from "lodash";
import { FindFunctionRanger, FindPositionFunction, GetFunctionDefined, GetFunctionNameInString, MatchVaribales, TextTrim } from "./utils";
import { FunctionDescription, FunctionRanger } from "./constants";

const cmd_lang = 'auto-tab-lang';
const lang_ext = "ats";

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "Auto-tab-script" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand(`${cmd_lang}.${lang_ext}`, () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		vscode.window.showInformationMessage(
			"Hello World from Auto tab script!"
		);
	});

	context.subscriptions.push(disposable);

	// 👍 formatter implemented using API
	vscode.languages.registerDocumentFormattingEditProvider(cmd_lang, {
		provideDocumentFormattingEdits(
			document: vscode.TextDocument
		): vscode.TextEdit[] {
			const lineCount = document.lineCount;
			const textEdit: vscode.TextEdit[] = [];

			for (let i = 0; i < lineCount; i++) {
				let line = document.lineAt(i);
				textEdit.push(
					vscode.TextEdit.replace(
						new vscode.Range(line.range.start, line.range.end),
						line.text.trim()
					)
				);
			}

			for (let i = 0; i < lineCount; i++) {
				let line = document.lineAt(i);
				let text = line.text.trim();
				let func = FindFunctionRanger(text);
				// console.log('func', func, i);
				if (func) {
					let findPos = FindPositionFunction({ start_line: i, start_script: func.start, end_script: func.end, lineCount, document });
					// console.log('findPos', func, findPos, i);
					for (let ii = i + 1; ii < findPos.end_line; ii++) {
						// console.log('document.lineAt(ii)', ii);
						textEdit.push(
							vscode.TextEdit.insert(document.lineAt(ii).range.start, "\t")
						);
					}
				}
			}

			return textEdit;
		},
	});

	// color in function
	const tokenTypes = ["class", "interface", "enum", "function", "variable"];
	const tokenModifiers = ["declaration", "documentation"];
	const legend = new vscode.SemanticTokensLegend(tokenTypes, tokenModifiers);

	vscode.languages.registerDocumentSemanticTokensProvider(
		{ language: cmd_lang, scheme: "file" },
		{
			provideDocumentSemanticTokens(
				document: vscode.TextDocument
			): vscode.ProviderResult<vscode.SemanticTokens> {
				// analyze the document and return semantic tokens
				const tokensBuilder = new vscode.SemanticTokensBuilder(legend);
				const lineCount = document.lineCount;

				for (let i = 0; i < lineCount; i++) {
					let line = document.lineAt(i);
					let text = line.text;

					let funcs = GetFunctionNameInString(text);

					funcs.map((func) => {
						tokensBuilder.push(
							new vscode.Range(new vscode.Position(i, text.indexOf(func)), new vscode.Position(i, text.indexOf(func) + func.length)),
							'function',
							['declaration']
						);
					})

					if(FunctionRanger.find((func) => func.end == TextTrim(text)) || TextTrim(text) == 'Break'){
						tokensBuilder.push(
							new vscode.Range(line.range.start, line.range.end),
							'function',
							['declaration']
						);
					}

					let varibales = MatchVaribales(text);

					varibales?.map((val) => {
						tokensBuilder.push(
							new vscode.Range(new vscode.Position(i, text.indexOf(val)), new vscode.Position(i, text.indexOf(val) + val.length)),
							'variable',
							['declaration']
						);
					})
				}
				return tokensBuilder.build();
			},
		},
		legend
	);

	// hover description
	vscode.languages.registerHoverProvider(cmd_lang, {
		provideHover(document, position, token) {
			const definedFuntions = GetFunctionDefined(document);

			const range = document.getWordRangeAtPosition(position);
			const word = document.getText(range);
			let value = (Object.assign(FunctionDescription, definedFuntions) as any)[word] as {
				title: string,
				content: string[]
			};

			if (value) {
				return new vscode.Hover({
					language: cmd_lang,
					value: [value.title+"\n"].concat(value.content).join("\n")
				});
			}
		}
	});

	// completion item
	vscode.languages.registerCompletionItemProvider(
		cmd_lang,
		{
			provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

				// get all text until the `position` and check if it reads `console.`
				// and if so then complete if `log`, `warn`, and `error`
				const linePrefix = document.lineAt(position).text.slice(0, position.character);
				const values : any = [];

				Object.keys(FunctionDescription).map((key) => {
					if(key.match(linePrefix.trim())){
						values.push(new vscode.CompletionItem(`${key}`, vscode.CompletionItemKind.Method));
					}
				})

				return values
			}
		}
	);

}

// This method is called when your extension is deactivated
export function deactivate() { }
