export const FunctionRanger = [
    {
        start: "If",
        end: "EndIf",
    },
    {
        start: "IfNot",
        end: "EndIfNot",
    },
    {
        start: "DefineFunc",
        end: "EndDefineFunc",
    },
    {
        start: "Loop",
        end: "EndLoop",
    },
    {
        start: "OpenApp",
        end: "EndOpenApp",
    },
    {
        start: "OpenThread",
        end: "EndOpenThread",
    },
];

export const FunctionDescription = {
    "If": {
        title: "(constructor) If(value: boolean): any",
        content: [
            "@param value - boolean",
            "@return - any"
        ]
    },
    "IfNot": {
        title: "(constructor) IfNot(value: boolean): any",
        content: [
            "@param value - boolean",
            "@return - any"
        ]
    },
    "Loop": {
        title: "(constructor) Loop(max, index): any",
        content: [
            "@param max - number | boolean",
            "@param index - string (param auto)",
            "@return - any"
        ]
    },
    "DefineFunc": {
        title: "(constructor) DefineFunc(name, n1, n2...): any",
        content: [
            "@param name - Function name",
            "@param n1,n2... - any (defined param in function)",
            "@return - any"
        ]
    },
    "OpenApp": {
        title: `(constructor) OpenApp(params: Object): any`,
        content: [
            "@param url - string",
            "@param width - number[]",
            "@param height - number[]",
            "@param isSync - boolean",
            "@param focused - boolean",
            "@param isClearSiteData - boolean",
            "@param isWindowDefault - boolean",
            "@param isCloseOnFinish - boolean",
            "@return - any"
        ]
    },
    "OpenThread": {
        title: "(constructor) OpenThread(params: Object): any",
        content: [
            "@param isSync - boolean",
            "@return - any"
        ]
    },
    "Is_Element": {
        title: "(method) Is_Element(xpath): boolean",
        content: [
            "@param xpath - string",
            "@return - boolean"
        ]
    },
    "Wait_Element": {
        title: "(method) Wait_Element(xpath, timeout): boolean",
        content: [
            "@param xpath - string",
            "@param timout - number",
            "@return - boolean"
        ]
    },
    "SetVar": {
        title: "(method) SetVar(name, value): any",
        content: [
            "@param name - string (name varibales)",
            "@param value - any",
            "@return - any"
        ]
    },
    "Is_Equal": {
        title: "(method) Is_Equal(val1, val2): boolean",
        content: [
            "@param val1 - string | boolean | number",
            "@param val2 - string | boolean | number",
            "@return - boolean"
        ]
    },
    "Is_Not_Equal": {
        title: "(method) Is_Not_Equal(val1, val2): boolean",
        content: [
            "@param val1 - string | boolean | number",
            "@param val2 - string | boolean | number",
            "@return - boolean"
        ]
    },
    "Is_Greater_Than": {
        title: "(method) Is_Greater_Than(val1, val2): boolean",
        content: [
            "@param val1 - string | boolean | number",
            "@param val2 - string | boolean | number",
            "@return - boolean"
        ]
    },
    "Is_Greater_Or_Than": {
        title: "(method) Is_Greater_Or_Than(val1, val2): boolean",
        content: [
            "@param val1 - string | boolean | number",
            "@param val2 - string | boolean | number",
            "@return - boolean"
        ]
    },
    "Is_Less_Than": {
        title: "(method) Is_Less_Than(val1, val2): boolean",
        content: [
            "@param val1 - string | boolean | number",
            "@param val2 - string | boolean | number",
            "@return - boolean"
        ]
    },
    "Is_Less_Or_Than": {
        title: "(method) Is_Less_Or_Than(val1, val2): boolean",
        content: [
            "@param val1 - string | boolean | number",
            "@param val2 - string | boolean | number",
            "@return - boolean"
        ]
    },
    "Is_String_Contains": {
        title: "(method) Is_String_Contains(val1, val2): boolean",
        content: [
            "@param val1 - string",
            "@param val2 - string",
            "@return - boolean"
        ]
    },
    "Read_Value_Obj": {
        title: "(method) Read_Value_Obj(value: Object, path: string): any",
        content: [
            "@param value - Object",
            "@param path - string",
            "@return - any"
        ]
    },
    "Get_Size": {
        title: "(method) Get_Size(value: []): number",
        content: [
            "@param value - Array",
            "@return - number"
        ]
    },
    "Str_To_Json": {
        title: "(method) Str_To_Json(value: string, num_encode?: number): json",
        content: [
            "@param value - string",
            "@param num_encode - number",
            "@return - json"
        ]
    },
    "Json_To_Str": {
        title: "(method) Json_To_Str(value: json, num_encode?: number): string",
        content: [
            "@param value - json",
            "@param num_encode - number",
            "@return - string"
        ]
    },
    "Merge_Obj": {
        title: "(method) Merge_Obj(n1, n2...): object",
        content: [
            "@param n1, n2.. - object",
            "@return - object"
        ]
    },
    "Lodash_": {
        title: "(method) Lodash_(type, n1, n2...): any",
        content: [
            "@param type - string",
            "@param n1, n2.. - any",
            "@return - any"
        ]
    },
    "Get": {
        title: "(method) Get(url, option?, headers?, timeout?): any",
        content: [
            "@param url - string",
            "@param option - \"json\" || \"text\"",
            "@param headers - object",
            "@param timeout - number",
            "@return - any"
        ]
    },
    "Post": {
        title: "(method) Post(url, option?, body?, headers?, timeout?): any",
        content: [
            "@param url - string",
            "@param option - \"json\" || \"text\"",
            "@param body - any",
            "@param headers - object",
            "@param timeout - number",
            "@return - any"
        ]
    },
    "Put": {
        title: "(method) Put(url, option?, body?, headers?, timeout?): any",
        content: [
            "@param url - string",
            "@param option - \"json\" || \"text\"",
            "@param body - any",
            "@param headers - object",
            "@param timeout - number",
            "@return - any"
        ]
    },
    "Sum_Value": {
        title: "(method) Sum_Value(n1, n2..): number",
        content: [
            "@param n1, n2.. - number",
            "@return - number"
        ]
    },
    "Init_Proxy": {
        title: "(method) Init_Proxy(key, type, timeout, bypass): any",
        content: [
            "@param key - string",
            "@param type - \"tinsoft\"",
            "@param timeout - number",
            "@param bypass - string[]",
            "@return - any"
        ]
    },
    "Set_Proxy": {
        title: "(method) Set_Proxy(proxy): boolean",
        content: [
            "@param proxy - string (ip:port:user:password)",
            "@return - boolean"
        ]
    },
    "Remove_Proxy": {
        title: "(method) Remove_Proxy(): boolean",
        content: [
            "@return - boolean"
        ]
    },
    "Close_Window": {
        title: "(method) Close_Window(): boolean",
        content: [
            "@return - boolean"
        ]
    },
    "Console_Log": {
        title: "(method) Console_Log(n1, n2..): any",
        content: [
            "@param n1, n2.. - any",
            "@return - any"
        ]
    },
    "Clear_SiteData": {
        title: "(method) Clear_SiteData(domain): boolean",
        content: [
            "@param domain - string",
            "@return - boolean"
        ]
    },
    "Get_Value_Cookie": {
        title: "(method) Get_Value_Cookie(url, field?, is_obj?)",
        content: [
            "@param url - string",
            "@param field - string",
            "@param is_obj - boolean",
            "@return - object | string"
        ]
    },
    "Set_Cookie": {
        title: "(method) Set_Cookie(url, domain, cookies): boolean",
        content: [
            "@param url - string",
            "@param domain - string",
            "@param cookies - string | object",
            "@return - boolean"
        ]
    },
    "Clear_Cookie": {
        title: "(method) Clear_Cookie(domain): boolean",
        content: [
            "@param domain - string",
            "@return - boolean"
        ]
    },
    "Sleep": {
        title: "(method) Sleep(timeout): boolean",
        content: [
            "@param timeout - number",
            "@return - boolean"
        ]
    },
    "Capture_Tab": {
        title: "(method) Capture_Tab(): string",
        content: [
            "@return - string"
        ]
    },
    "Notification": {
        title: "(method) Notification(message): boolean",
        content: [
            "@param message - string",
            "@return - boolean"
        ]
    },
    "GenerateToken2fa": {
        title: "(method) GenerateToken2fa(seret_2fa): string",
        content: [
            "@param seret_2fa - string",
            "@return - string"
        ]
    },
    "Random_User_Name": {
        title: "(method) Random_User_Name(length, is_date?): string",
        content: [
            "@param length - number",
            "@param is_date - boolean",
            "@return - string"
        ]
    },
    "Random_User_Name_From_Name": {
        title: "(method) Random_User_Name_From_Name(value): string",
        content: [
            "@param value - string",
            "@return - string"
        ]
    },
    "Random_Full_Name": {
        title: "(method) Random_Full_Name(country, gender?): string",
        content: [
            "@param country - vn | string",
            "@param gender - number",
            "@return - string"
        ]
    },
    "Get_First_Name": {
        title: "(method) Get_First_Name(value): string",
        content: [
            "@param value - string",
            "@return - string"
        ]
    },
    "Get_Last_Name": {
        title: "(method) Get_Last_Name(value): string",
        content: [
            "@param value - string",
            "@return - string"
        ]
    },
    "Random_Password": {
        title: "(method) Random_Password(value): string",
        content: [
            "@param value - number",
            "@return - string"
        ]
    },
    "Random_Date": {
        title: "(method) Random_Date(startDate: string, endDate: string): string",
        content: [
            "@param startDate - string",
            "@param endDate - string",
            "@return - string"
        ]
    },
    "Random_Number": {
        title: "(method) Random_Number(min, max): number",
        content: [
            "@param value - number",
            "@param value - number",
            "@return - number"
        ]
    },
    "Random_Values": {
        title: "(method) Random_Values(n1, n2..): any",
        content: [
            "@param n1, n2 - any",
            "@return - any"
        ]
    },
    "Random_Value_In_Array": {
        title: "(method) Random_Value_In_Array(value): any",
        content: [
            "@param value - array",
            "@return - any"
        ]
    },
    "Get_Date_Format": {
        title: "(method) Get_Date_Format(date, format): string",
        content: [
            "@param date - string",
            "@param format - string",
            "@return - string"
        ]
    },
    "GetUserAgent": {
        title: "(method) GetUserAgent(): string",
        content: [
            "@return - string"
        ]
    },
    "Throw": {
        title: "(method) Throw(any): any",
        content: [
            "@param value - any",
            "@return - any"
        ]
    },
    "Tabs_Query": {
        title: "(method) Tabs_Query(query): any",
        content: [
            "@param query - option chrome deverloper",
            "@return - any"
        ]
    },
    "Tabs_Update": {
        title: "(method) Tabs_Update(tabId, updateProoerties): any",
        content: [
            "@param tabId - number",
            "@param updateProoerties - any",
            "@return - any"
        ]
    },
    "Get_File_Base64": {
        title: "(method) Get_File_Base64(url): string",
        content: [
            "@param url - string",
            "@return - string"
        ]
    },
    "ToUpperCase": {
        title: "(method) ToUpperCase(value): string",
        content: [
            "@param value - string",
            "@return - string"
        ]
    },
    "ToLowerCase": {
        title: "(method) ToLowerCase(value): string",
        content: [
            "@param value - string",
            "@return - string"
        ]
    },
    "Macth_Text": {
        title: "(method) Macth_Text(text, regex, flags): any",
        content: [
            "@param text - string",
            "@param regex - string",
            "@param flags - string",
            "@return - flags"
        ]
    },
    "Macth_All_Text": {
        title: "(method) Macth_All_Text(text, regex, flags): any",
        content: [
            "@param text - string",
            "@param regex - string",
            "@param flags - string",
            "@return - flags"
        ]
    },
    "Go_To_Url": {
        title: "(method) Go_To_Url(url, active?): boolean",
        content: [
            "@param url - string",
            "@param active - boolean",
            "@return - boolean"
        ]
    },
    "Active_Tab": {
        title: "(method) Active_Tab(): boolean",
        content: [
            "@return - boolean"
        ]
    },
    "Close_Tab": {
        title: "(method) Close_Tab(): boolean",
        content: [
            "@return - boolean"
        ]
    },
    "Reload_Tab": {
        title: "(method) Reload_Tab(): boolean",
        content: [
            "@return - boolean"
        ]
    },
    "Set_TabId": {
        title: "(method) Set_TabId(tabId): boolean",
        content: [
            "@param tabId - number",
            "@return - boolean"
        ]
    },
    "Get_Status_Tab": {
        title: "(method) Get_Status_Tab(): any",
        content: [
            "@return - any"
        ]
    },
    "Notification_Tab": {
        title: "(method) Notification_Tab(text, tabId): any",
        content: [
            "@param text - string",
            "@param tabId - string",
            "@return - any"
        ]
    },
    "Click_Element": {
        title: "(method) Click_Element(xpath, debug?): boolean",
        content: [
            "@param xpath - string",
            "@param debug - boolean",
            "@return - boolean"
        ]
    },
    "Input_Element": {
        title: "(method) Input_Element(xpath, value): boolean",
        content: [
            "@param xpath - string",
            "@param value - string | number",
            "@return - boolean"
        ]
    },
    "Input_Element2": {
        title: "(method) Input_Element2(xpath, value): boolean",
        content: [
            "@param xpath - string",
            "@param value - string | number",
            "@return - boolean"
        ]
    },
    "Wait_Location": {
        title: "(method) Wait_Location(location, type, timeout): boolean",
        content: [
            "@param location - string",
            "@param type - string",
            "@param timeout - number",
            "@return - boolean"
        ]
    },
    "Write_Log": {
        title: "(method) Write_Log(n1, n2..): any",
        content: [
            "@return - any"
        ]
    },
    "Scroll_To": {
        title: "(method) Scroll_To(x, y, xpath?): boolean",
        content: [
            "@param x - number",
            "@param y - number | \"bottom\"",
            "@param xpath - string",
            "@return - boolean"
        ]
    },
    "Element_Scroll_To": {
        title: "(method) Element_Scroll_To(x, y, xpath?): boolean",
        content: [
            "@param x - number",
            "@param y - number | \"bottom\"",
            "@param xpath - string",
            "@return - boolean"
        ]
    },
    "Get_Of_Xpath": {
        title: "(method) Get_Of_Xpath(xpath, type, name): any",
        content: [
            "@param xpath - string",
            "@param type - 1 | 2 | 3",
            "@param name - string (name attribute)",
            "@return - any"
        ]
    },
    "Stop": {
        title: "(method) Stop(): boolean",
        content: [
            "@return - boolean"
        ]
    },
    "Return": {
        title: "(method) Return(value): any",
        content: [
            "@param value - any",
            "@return - any"
        ]
    },
    "Get_Location_Tab": {
        title: "(method) Get_Location_Tab(type): string",
        content: [
            "@param type - string | full_url",
            "@return - string"
        ]
    },
    "Mouse_Event": {
        title: "(method) Mouse_Event(xpath, event, option?): boolean",
        content: [
            "@param xpath - string",
            "@param event - string",
            "@param option - object",
            "@return - boolean"
        ]
    },
    "Pointer_Event": {
        title: "(method) Pointer_Event(xpath, event, option?): boolean",
        content: [
            "@param xpath - string",
            "@param event - string",
            "@param option - object",
            "@return - boolean"
        ]
    },
    "Input_Event": {
        title: "(method) Input_Event(xpath, event, option?): boolean",
        content: [
            "@param xpath - string",
            "@param event - string",
            "@param option - object",
            "@return - boolean"
        ]
    },
    "Mouse_Hover": {
        title: "(method) Mouse_Hover(xpath, timout): boolean",
        content: [
            "@param xpath - string",
            "@param timout - number",
            "@return - boolean"
        ]
    },
    "Focus_Element": {
        title: "(method) Focus_Element(xpath): boolean",
        content: [
            "@param xpath - string",
            "@return - boolean"
        ]
    },
    "Blur_Element": {
        title: "(method) Blur_Element(xpath): boolean",
        content: [
            "@param xpath - string",
            "@return - boolean"
        ]
    },
    "Find_Element": {
        title: "(method) Find_Element(xpath, position, values, type): any",
        content: [
            "@param xpath - string",
            "@param position - any",
            "@param values - any",
            "@param type - dom | index",
            "@return - any"
        ]
    },
    "Get_Position_Element": {
        title: "(method) Get_Position_Element(xpath): any",
        content: [
            "@param xpath - string",
            "@return - any"
        ]
    },
    "Handle_Input_File": {
        title: "(method) Handle_Input_File(xpath, url): boolean",
        content: [
            "@param xpath - string",
            "@param url - string",
            "@return - boolean"
        ]
    },
}